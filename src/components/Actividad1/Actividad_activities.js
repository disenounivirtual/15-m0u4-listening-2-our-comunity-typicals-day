const activities = [
{
    "id":1,
    "act": "Wake up",
    "audio": "./media/audio.mp3"

},
{
    "id":2,
    "act": "Get up",
    "audio": "./media/audio.mp3"

},
{
    "id":3,
    "act": "Take a shower",
    "audio": "./media/audio.mp3"

},
{
    "id":4,
    "act": "Have class",
    "audio": "./media/audio.mp3"

},
{
    "id":5,
    "act": "Teach a lesson",
    "audio": "./media/audio.mp3"

},
{
    "id":6,
    "act": "Work",
    "audio": "./media/audio.mp3"

},
{
    "id":7,
    "act": "Study",
    "audio": "./media/audio.mp3"

},
{
    "id":8,
    "act": "Eat",
    "audio": "./media/audio.mp3"

},
{
    "id":9,
    "act": "Drink",
    "audio": "./media/audio.mp3"

},
{
    "id":10,
    "act": "Watch TV",
    "audio": "./media/audio.mp3"

},
{
    "id":11,
    "act": "Listen to music",
    "audio": "./media/audio.mp3"

},
{
    "id":12,
    "act": "Sleep",
    "audio": "./media/audio.mp3"

},
]
export default activities